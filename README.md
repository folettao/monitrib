﻿# MONIT-RIB – TECNOLOGIA DA INFORMAÇÃO DE COMUNICAÇÃO PARA MONITORAMENTO DE RIBEIRÕES EM CASOS DE CHEIAS
Projeto referente ao Edital nº 12/2019 do Instituto Federal Catarinense, onde 
propõe realizar medições de nível (água) de Ribeirões para auxílio na prevenção 
de cheias no Alto Vale do Itajaí.

# Resumo
Eventos climáticos afetam, com maior frequência, as populações próximas de Ribeirões,
pois possuem comportamentos diversos aos dos rios. Atualmente, esse rios encontram-se
monitorados pela Defesa Civil, que emite alertas à população periodicamente. Com isso,
gerou este novo problema de pesquisa, onde é possível expandir e aprimorar técnicas
para medição de nível de água por meio de dispositivos vinculados ao conceito de internet
das coisas (IoT). Por serem compactos, dinâmicos e consumirem pouca energia, tendem
a ganhar emprego em protótipos pequenos e de baixa necessidade de manutenção.que permite 
a instalação em ambientes de difícil ou limitado acesso. Foram utilizados um
mini microcomputador, sensor ultrassônico e uma câmera. Após testes e discussões,
pode-se verificar que o protótipo experimental é permite agregar aos métodos tradicionais
de aquisição destes dados, garantindo precisão, persistência, acesso fácil aos mesmos.
Durante os testes, verificou-se que a água translúcida gera problemas de aquisição dos
dados. Para trabalhos futuros o projeto, versão 2 pretende expandir os pontos de coleta
para diferentes locais no leito em que corre o Ribeirão Fundo Canoas, permitindo melhor
prognóstico de aumento de seu nível nestes diferentes pontos.

# Agradecimentos
Agradeço ao IFC Propi – Pró-reitoria Pesquisa e Inovação e Direção Campus Rio do Sul e 
CNPQ/MEC, pela concessão da bolsa e auxílio para aquisição dos materiais. Ao professor 
Jó Ueyama (USP), pelo pelos ensinamentos e contribuições para o projeto. Ao analista de 
TI do IFC – UU, Fabiano, pela paciência e auxílio ao projeto. Ao time do projeto, pela 
parceria. Ao coordenador de projeto, Fábio Alexandrini, pela oportunidade confiada. 
