Inserir no crontab da raspberry para que as imagens sejam deletadas, os logs do sistema sejam apagados e o programa seja reiniciado a cada 
dia ao meio dia (12:00).

```bash 
# crontab -e 
```

        *  *    * * 0   journalctl --vacuum-time=7d
        *  *    * * 0   journalctl --vacuum-size=200M

        *  * 1,15 * *   find /opt/monitrib/image/transferred/ -mtime +15 -exec rm -fv {} \; 
        *  * 1,15 * *   find /opt/monitrib/image/error/ -mtime +30 -exec rm -fv {} \;

        *  * 1,15 * *   systemctl restart monitrib_code_main.service 
 
