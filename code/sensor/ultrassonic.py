﻿# -*- coding: utf-8 -*

## Start Import section
from serialPort import SerialPort

from log import log_txt as log
## End Import section


class DataCollect():

    ## Start Declaration static variable section
    serial = SerialPort()
    __timeOutCollect = 30 # seconds
    ## End Declaration static variable section

    def __init__(self, sensorHeight = None):
        self.sensorHeight = sensorHeight ## distance between sensor and floor

    ## Collect data with TfMini Plus
    def __collect(self):
        try:
            DataCollect.serial.open()
            count = DataCollect.serial.ser.in_waiting
            if count > 8:
                recv = DataCollect.serial.ser.read(9)
                DataCollect.serial.ser.reset_input_buffer()
                if recv[0] == 'Y' and recv[1] == 'Y':
                    low = int(recv[2].encode('hex'), 16)
                    high = int(recv[3].encode('hex'), 16)
                    return self.sensorHeight - (low + high * 256) \
                        if (self.sensorHeight is not None) else (low + high * 256)
            return -1
        except:
            return -1

    ## Valid collect data
    def get(self):
        distance = -1
        startCollect = self.__getTimeNow()
        while distance == -1:
            distance = self.__collect()
            if self.__calcTime(startCollect, self.__getTimeNow()) > DataCollect.__timeOutCollect:
                break
            
        if distance is not -1:
            print(log.DataCollect.sucess(distance))
        else:
            print(log.DataCollect.fail())

        self.close()
        return distance

    ### Close connection with serial
    @staticmethod
    def close():
        DataCollect.serial.close()

    @staticmethod
    def __getTimeNow():
        import time
        return time.time()
    
    @staticmethod
    def __calcTime(start, end):
        return (end - start)# return time in seconds
