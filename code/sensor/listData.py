﻿# -*- coding: utf-8 -*

class ListData():
    ## Start Declaration variable section
    list = []
    ## End Declaration variable section

    def __init__(self):
        pass

    ## Add data in list
    def add(self, data, decimalPlace):
        if (data is not None or data > 0):
            ListData.list.append(round(data, decimalPlace))

    ## Return size of list
    def size(self):
        return len(ListData.list)

    ## Clean list
    def clean(self):
        del ListData.list[:]

    ## Get all list
    def get(self):
        return ListData.list
