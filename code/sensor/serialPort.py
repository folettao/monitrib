﻿# -*- coding: utf-8 -*

## Start Import section
import serial
import time

from log import log_txt as log
## End Import section

class SerialPort(object):

    ## Start Declaration static variable section
    ser = serial.Serial('/dev/ttyAMA0', 115200)  ## Open serial communication
    # ## End Declaration static variable section

    ## Open serial port connection
    @staticmethod
    def open():
        try:
            while SerialPort.ser.is_open is False:
                SerialPort.ser.open()
            log.SerialPort.sucessOpenPort()
        except:
            SerialPort.close()
            log.SerialPort.failOpenPort()

    ## Close serial port connection
    @staticmethod
    def close():
        if SerialPort.ser is not None:
            SerialPort.ser.close()
            log.SerialPort.closePort()
