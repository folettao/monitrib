﻿# -*- coding: utf-8 -*

## Start Import section
import time
#import cv2
import os

from log import log_txt as log
## End Import section


class Camera():

    ## Start Declaration static variable section
    servData = ''
    ## End Declaration static variable section

    def __init__(self, path, servUser, servIP, imageName, imageExtension, rtspProtocol = None):
        self.rtsp       = rtspProtocol
        Camera.servData = '{}@{}:{}'.format(servUser, servIP, path)
        self.imageName  = '{}_{}.{}'.format(time.strftime('%Y%m%d-%H%M%S'), imageName, imageExtension)
        self.fileData   = '{}{}'.format(path, self.imageName)


    ## it can be rtsp or http stream
    def openRTSP(self):
        conn = cv2.VideoCapture(self.rtsp)

        if conn.isOpened():
            print(log.Camera.sucessOpenStream())
            return conn
        print(log.Camera.failOpenStream())
        return False

    ## take shot in rasp native camera 
    def shotNative(self):
        try:
            os.system('raspistill -o {}'.format(self.fileData))
            print(log.Camera.sucessShot(self.fileData))
            return self.imageName
        except:
            print(log.Camera.failShot())
            return False

    def shotRTSP(self):
        conn = self.openRTSP()

        if conn is not False:
            _, frame = conn.read()
            if _ and frame is not None:
                cv2.imwrite(self.fileData, frame)
                conn.release()          ### releasing camera immediately after capturing picture
                cv2.destroyAllWindows() ### destroy all windows create by cv2
                print(log.Camera.sucessShot(self.fileData))
                return self.fileData
        print(log.Camera.failShot())
        return False

    def getImageName(self):
        return self.imageName
