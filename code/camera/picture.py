# -*- coding: utf-8 -*

## Start Import section
import os

from camera import takeShot
from log    import log_txt as log
## End Import section

class Manip():

    ## Start Declaration static variable section
    trySend = 0
    servDataSSH = ''
    camera = None
    folderImageTransferred = 'transferred'
    folderImageTransferredError = 'error'
    pathMain = ''
    ## End Declaration static variable section

    def __init__(self, path, servUser, servIP, imageName, imageExtension, trySend = 5):
        Manip.camera = takeShot.Camera(
                                path,
                                servUser,
                                servIP,
                                imageName,
                                imageExtension)
        Manip.servDataSSH  = 'ssh {}@{} ls -l'.format(servUser, servIP)
        Manip.trySend = trySend  # Try send image to server (SSH)
        Manip.pathMain = path

    @staticmethod
    def transferImage(imageData, trySend):
        bashReturn = os.system('scp {}{} {}'.format(
            Manip.pathMain
            , imageName
            , Manip.camera.servData
            )
        )

        if bashReturn is 0:
            Manip.moveImageHostTransferred(imageName)
            return trySend
        else:
            if trySend < Manip.trySend:
                return Manip.transferImage(imageData, trySend + 1)
        Manip.moveImageHostTransferredError(imageData)
        return trySend


    @staticmethod
    def transferOneImage():
        imageName = Manip.camera.getImageName()

        bashReturn = os.system('scp {}{} {}'.format(
            Manip.pathMain
            , imageName
            , Manip.camera.servData
            )
        )
        if bashReturn is 0:
            Manip.moveImageHostTransferred(imageName)
            print(log.Camera.sucessTransferImage(imageName, Manip.camera.servData))
        else:
            trySend = Manip.transferImage(imageName, 1)
            if trySend != Manip.trySend:
                print(log.Camera.sucessTransferImage(imageData, Manip.camera.servData))
            else:
                print(log.Camera.failTransferImage(trySend, imageName, Manip.camera.servData))



    @staticmethod
    def transferAllImage():
        for imageName in Manip.__lsAwk():
            bashReturn = os.system('scp {}{} {}'.format(
                Manip.pathMain
                , imageName
                , Manip.camera.servData
                )
            )
            if bashReturn is 0:
                Manip.moveImageHostTransferred(imageName)
                print(log.Camera.sucessTransferImage(imageName, Manip.camera.servData))
            else:
                trySend = Manip.transferImage(imageName, 1)
                if trySend != Manip.trySend:
                    print(log.Camera.sucessTransferImage(imageData, Manip.camera.servData))
                else:
                    print(log.Camera.failTransferImage(trySend, imageName, Manip.camera.servData))

    @staticmethod
    def checkImageHostTransferred(imageData):
        return (os.system('{} {}'.format(Manip.servDataSSH, imageData)) is 0 and True or False)

    @staticmethod
    def __lsAwk():
        text = 'ls -lt  --ignore={} --ignore={} {}'.format(
            Manip.folderImageTransferred
            , Manip.folderImageTransferredError
            , Manip.pathMain
        )
        text = text + " | awk '{print $9}'"
        return os.popen(text).read()[1:-1].split('\n')

    @staticmethod
    def checkImageExist(fileName):
        for imageName in Manip.__lsAwk():
            if imageName in fileName:
                return True
        return False


    @staticmethod
    def moveImageHostTransferred(imageData):
        if Manip.checkImageExist(imageData):
            mvReturn = os.system('mv {0}{1} {0}{2}'.format(
                Manip.pathMain
                , imageData
                , Manip.folderImageTransferred
                )
            )
            if mvReturn is 0:
                print(log.Camera.sucessMoveImage(imageData, Manip.folderImageTransferred))
                return
            print(log.Camera.failMoveImage(imageData, Manip.folderImageTransferred))

    @staticmethod
    def moveImageHostTransferredError(imageData):
        mvReturn = os.system('mv {0}{1} {0}{2}'.format(
            Manip.pathMain
            , imageData
            , Manip.folderImageTransferredError
            )
        )
        if mvReturn is 0:
            print(log.Camera.sucessMoveImage(imageData, Manip.folderImageTransferredError))
        print(log.Camera.failMoveImage(imageData, Manip.folderImageTransferredError))

    """ 
    Remove files over more older with set date (days) in function
    """
    @staticmethod
    def deleteImageHostLastData(days):
        # Pplus 5 (+5) means: files have > 5 dais are removed
        rmReturn = os.system('find {}{}/ -mtime +{} -exec rm -fv {} \;'.format(
            Manip.pathMain
            , Manip.folderImageTransferred
            , days
            , '{}'
            )
        )
        if rmReturn is 0:
            print(log.Camera.sucessDeleteImageHost(imageData))
        print(log.Camera.failDeleteImageHost(imageData))