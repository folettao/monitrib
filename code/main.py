﻿# -*- coding: utf-8 -*

# create folder in first run code:
#                   mkdir -p /opt/monitrib/image/transferred
#                   mkdir -p /opt/monitrib/image/error
#                   mkdir -p /opt/monitrib/code/dao/sql_error

## Start Import section
import time
import threading

from camera.picture     import Picture
from sensor.ultrassonic import DataCollect
from sensor.listData    import ListData
from dao.dao            import Insert
from dao.dao            import Connect
from log.log_txt        import Dispositivo
## End Import section


class Main():
    ## Start Declaration variable section
    timeSleep    = 60  ## Time in seconds (waiting to get size)
    sizeList     = 30  ## List have sizes listSize (median)
    decimalPlace = 3
    sensorHeight = 162
    days = 15

    # Camera variable
    path           = '/opt/monitrib/image/'     # Path to save pictures
    servUser       = ''            # User name in serv (connect with ssh)
    servIP         = ''            # IP of server 
    imageName      = 'rasp_01'                  # Default name for pictures
    imageExtension = 'png'                      # Extension to pictures
    # Enable for active a rtsp communication
#    RTSP           = 'rtsp://admin:Qwert123@10.1.1.21:554/cam/realmonitor?channel=1&subtype=0'

    ## End Declaration variable section

    def __init__(self):
        pass

    def __takePictureFogOFF(self, cam):
        """
        Executa thread apenas tentando enviar uma unica vez para o servidor. Caso retorne erro,
        a imange é movida para pasta 'error'.

        :param cam: Object
        """
        return threading.Thread(
            target = cam.transferOneImage
            , name = 'camThread'
        )

    def __takePictureFogON(self, cam):
        """
        Executa thread apenas tentando enviar uma unica vez para o servidor. Caso retorne erro,
        são realizadas algumas tentantivas para reenviá-la. Caso não consiga, a imange é movida
        para pasta 'error', caso exista sucesso no envio, a imagem é movida para pasta 'transferred'

        :param cam: Object
        """
        return threading.Thread(
            target = cam.transferAllImage
            , name = 'camThread'
        )

    def __takePicture(self, fog):
        """
        Estância o objeto e realiza a chamada do método.

        :param fog: boolean
        :return: string : nome da imagem formatado.
        """
        cam = Picture(
            Main.path
            , Main.servUser
            , Main.servIP
            , Main.imageName
            , Main.imageExtension
            # Enable for active a rtsp communication
            #, Main.RTSP
        )
        #picture = cam.camera.shotRTSP()
        picture = cam.camera.shotNative()
        if picture is not False:
            if fog: #fog on
                self.__takePictureFogON(cam).start()
            else: #fog off
                self.__takePictureFogOFF(cam).start()
        return picture

    def __databaseInsert(self, fog, dataList, pictureName):
        """
        Realiza insert no banco de dados, seja no servidor (Cloud) ou localhost. Tudo depende do estado
        da fog: True/False.

        :param fog: boolean
        :param pictureName: string : nome da imagem formatado.
        """
        bdInsert = Insert(dataList.get(), pictureName)

        if fog: #fog on
            # Test connection to DB (return boolean)
            # Connect.validConnectionLocalhostDatabase()
            bdInsertLocalhostThread = threading.Thread(target=bdInsert.sendLocalhost, name='bdInsertLocalhostThread')
            bdInsertLocalhostThread.start()

        #fog off
        # Test connection to DB (return boolean)
        # Connect.validConnectionServerDatabase()
        bdInsertServerThread = threading.Thread(target=bdInsert.sendServer, name='bdInsertServerThread')
        bdInsertServerThread.start()

    def __fog(self, dataList, fog = True):
        """
        Chamda de métodos para tirar foto e persistir no banco de dados.

        :param dataList: List : data sensor
        :param fog: boolean
        """
        pictureName = self.__takePicture(fog)
        self.__databaseInsert(fog, dataList, pictureName)

    def start(self):
        """
        Chamada captura de dados - sensore ultrassônico.
        Chamada caputra de dados - sensore fotográfico.
        Chamada métodos persistência banco de dados.
        """

        #collector = DataCollect(Main.sensorHeight)
        collector = DataCollect()
        dataList = ListData()

        while True:
            time.sleep(Main.timeSleep) # Sleep
            # Tratar sequencia dados não coletados
            dataList.add(collector.get(), Main.decimalPlace)

            if dataList.size() is Main.sizeList:
                self.__fog(dataList, False)  #Fog off
                #self.__fog(dataList, True) #Fog on
                dataList.clean() #clean list of data sensor

if __name__ == '__main__':
    Dispositivo.startAplicacao()
    Main().start()
    Dispositivo.closeAplicacao()

