from configparser import ConfigParser

class Mysql(object):

    ## Start Declaration static variable section
    db = {}
    ## End Declaration static variable section

    @staticmethod
    def configLocalhost(path='/opt/monitrib/', fileName='configLocalhost.ini', section='mysql'):
        # create parser and read ini configuration file
        parser = ConfigParser()
        parser.read(path + fileName)

        if parser.has_section(section):
            items = parser.items(section)
            for item in items:
                Mysql.db[item[0]] = item[1]
        else:
            raise Exception('{0} not found in the {1} file'.format(section, (path + fileName)))

        return Mysql.db

    @staticmethod
    def configServer(path='/opt/monitrib/', fileName='configServer.ini', section='mysql'):
        # create parser and read ini configuration file
        parser = ConfigParser()
        parser.read(path + fileName)

        if parser.has_section(section):
            items = parser.items(section)
            for item in items:
                Mysql.db[item[0]] = item[1]
        else:
            raise Exception('{0} not found in the {1} file'.format(section, (path + fileName)))

        return Mysql.db
