# -*- coding: utf-8 -*

## Start Import section
from mysql.connector import MySQLConnection, Error

from dbConfig import Mysql
from log      import log_txt as log
## End Import section


class DB(object):
    ## Start Declaration variable section
    conn = None                          ## Connector to database.
    ## End Declaration variable section

    @staticmethod
    def connectLocalhost():
        return DB.__connect(Mysql.configLocalhost())

    @staticmethod
    def connectServer():
        return DB.__connect(Mysql.configServer())

    ## Start MySQL connect
    @staticmethod
    def __connect(dbConfig):
        try:
            DB.conn = MySQLConnection(**dbConfig)
            if DB.conn.is_connected():
                print(log.Database.sucessConnectEstablished())
                return DB.conn.cursor()
            else:
                print(log.Database.failConnect())
        except Error as error:
            print(log.Database.failConnect(error))

    ## Close MySQL conenct
    @staticmethod
    def close():
        if DB.conn is not None and DB.conn.is_connected():
            DB.conn.close()
            print(log.Database.closeDatabase())
