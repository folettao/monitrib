﻿# -*- coding: utf-8 -*

## Start Import section
from statistics import median

from datetime import datetime
from mysql.connector import Error

from connect import DB
from log     import log_txt as log
## End Import section

## Query to insert data in MySQL database
class Insert():

    ## Start Declaration static variable section
    __database = 'monitrib'
    __table    = 'test'

    __query = "INSERT INTO {0}.{1}{2} VALUES{3}".format(
        __database
        , __table
        , "(value, dataInRasp, image_path)" # Names
        , "('{}', '{}', '{}')"              # Values
    )

    __path = '/opt/monitrib/code/dao/sql_error/'
    __name = '{}_insert'.format(datetime.now().strftime('%Y%m%d'))
    __ext  = 'sql'
    __fullPathAndName = '{}{}.{}'.format(__path, __name, __ext)

    ## End Declaration static variable section

    def __init__(self, value, imagePath):
        self.__query = self.__query.format(
            median(value)
            , datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            , self.__validPicture(imagePath)
        )

    def sendLocalhost(self):
        connector = DB()
        cur = connector.connectLocalhost()
        if connector.conn is not None and connector.conn.is_connected():
            try:
                cur.execute(self.__query)
                connector.conn.commit()
                print(log.Database.sucessSendData(self.__query))
            except Error as err:
                print(log.Database.failSendData(err.sqlstate))
            finally:
                cur.close()
                connector.close()
        else:
            print(log.Database.connectionDown)
            connector.close()

    def sendServer(self):
        connector = DB()
        cur = connector.connectServer()
        
        Insert.__checkFileExist()
        
        queryList = self.__leArquivoSQL()
        queryList.append(self.__query)
        if connector.conn is not None and connector.conn.is_connected():
            for query in queryList:
                try:
                    cur.execute(query)
                    connector.conn.commit()
                    print(log.Database.sucessSendData(query))
                except Error as err:
                    print(log.Database.failSendData(err.sqlstate))
                    self.__abreGravaArquivoSQL(query)
                finally:
                    cur.close()
                    connector.close()
        else:
            print(log.Database.connectionDown)
            connector.close()
            self.__abreGravaArquivoSQL(queryList)

    @staticmethod
    def __criaArquivoSQLVazio():
        with open(Insert.__fullPathAndName, 'w') as arq:
            arq.close()
            return True
        return Insert.__criaFolder()

    @staticmethod
    def __leArquivoSQL():
        with open(Insert.__fullPathAndName, "r") as arq:
            linhas = arq.readlines()
            if linhas:
                swap = []
                for linha in linhas:
                    swap.append(linha[:-1])
                Insert.__criaArquivoSQLVazio()
                return swap
        return []

    @staticmethod
    def __abreGravaArquivoSQL(linhas):
        with open(Insert.__fullPathAndName, 'a') as arq:
            if linhas is list:
                for linha in linhas:
                    arq.write(linha + '\n')
            else:
                arq.write(linhas + '\n')
            arq.close()
            return True
        return False

    @staticmethod
    ## Valid if picture is real
    def __validPicture(imagePath):
        return imagePath if (imagePath is not False) else 'camera fail'

    @staticmethod
    def __criaFolder():
        import os
        osReturn = os.system('mkdir -p {}'.format(Insert.__fullPathAndName))
        if osReturn is 0:
            log.Folder.failFolderCreateSQLError()
            return Insert.__criaArquivoSQLVazio()
        return log.Folder.sucessFolderCreateSQLError(Insert.__fullPathAndName)

    @staticmethod
    def __checkFileExist():
        import os.path
        if os.path.isfile(Insert.__fullPathAndName): # File exist
            return True
        Insert.__criaArquivoSQLVazio()
            
class Connect(object):
    @staticmethod
    ## Valid connection DB Localhost
    def validConnectionLocalhostDatabase():
        connector = DB()
        cur = connector.connect()
        return Connect.valid(connector, cur)

    @staticmethod
    ## Valid connection DB Server (cloud)
    def validConnectionServerDatabase():
        connector = DB()
        cur = connector.connect()
        return Connect.valid(connector, cur)

    @staticmethod
    ## Valid connection
    def valid(connector, cur):
        status = False
        if connector.conn is not None and connector.conn.is_connected():
            try:
                status = True
            except:
                status = False
            finally:
                cur.close()
                connector.close()
        else:
            connector.close()
        return status
