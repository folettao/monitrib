﻿# -*- coding: utf-8 -*

## Start Import section
import logging

from datetime import datetime
## End Import section

def head():
    '''__path      = '/var/log/monitrib/'
    __extension = 'log'
    __name      = '{}{}_Script_rasp01.{}'.format(
        __path
        , datetime.now().strftime('%Y%m%d')
        , __extension
    )
    logging.basicConfig(filename = '{}{}{}'.format*(__path + __name + __extension,
                        filemode = 'a',
                        format   = '%(asctime)s; %(name)s; %(levelname)s; %(message)s',
                        level    = logging.INFO)'''
    path      = '/var/log/monitrib/'
    extension = '.log'
    name      = '%s_Script_rasp01' % (datetime.now().strftime('%Y%m%d'))

    logging.basicConfig(filename = path + name + extension,
                        filemode = 'a',
                        format   = '%(asctime)s; %(name)s; %(levelname)s; %(message)s',
                        level    = logging.INFO)

# Disposivo
class Dispositivo(object):

    @staticmethod
    def startAplicacao():
        head()
        text = 'Dispositivo; Sucesso; Inicializando aplicação'
        logging.info(text)
        return text

    @staticmethod
    def closeAplicacao():
        head()
        text = 'Dispositivo; Sucesso; Finalizando aplicação'
        logging.info(text)
        return text

# Database
class Database(object):

    ### Outhers
    @staticmethod
    def closeDatabase():
        head()
        text = 'Database; Sucesso; Connect close database'
        logging.info(text)
        return text

    @staticmethod
    def connectionDown():
        head()
        text = 'Database; Fail; Connect down'
        logging.error(text)
        return text

    ### Sucess
    @staticmethod
    def sucessConnectEstablished():
        head()
        text = 'Database; Sucesso; Connect established database'
        logging.info(text)
        return text

    @staticmethod
    def sucessSendData(query):
        head()
        text = 'Database; Sucesso; Send data to database - {}'.format(query)
        logging.info(text)
        return text

    @staticmethod
    def sucessRecSQLError(query, path):
        head()
        text = 'Database; Sucess; Query error ({}) save on file - {}'.format(query, path)
        logging.info(text)
        return text

    ### Fail / Error
    @staticmethod
    def failConnect(string = ''):
        head()
        text = 'Database; Fail; Connect failed database - {}'.format(string)
        logging.error(text)
        return text

    @staticmethod
    def failSendData(sqlstate):
        head()
        text = 'Database; Fail; To send data to database - SQLSTATE: {}'.format(sqlstate)
        logging.error(text)
        return text

    @staticmethod
    def sucessRecSQLError(query, path):
        head()
        text = 'Database; Fail; Query error ({}) save on file - {}'.format(query, path)
        logging.error(text)
        return text

    @staticmethod
    def dbConectionTimeout110():
        head()

# Camera
class Camera(object):

    ### Sucess
    @staticmethod
    def sucessShot(fileData):
        head()
        text = 'Camera; Sucess; shot - {}'.format(fileData)
        logging.info(text)
        return text

    @staticmethod
    def sucessTransferImage(fileData, servData):
        head()
        text = 'Camera; Sucess; transfer image ({}) to server - {}'.format(fileData, servData)
        logging.info(text)
        return text

    @staticmethod
    def sucessDeleteImageHost(fileData):
        head()
        text = 'Camera; Sucess; delete image ({}) on host'.format(fileData)
        logging.info(text)
        return text

    @staticmethod
    def sucessOpenStream():
        head()
        text = 'Camera; Sucess; open stream'
        logging.info(text)
        return text

    @staticmethod
    def sucessMoveImage(name, path):
        head()
        text = 'Camera; Sucess; Transfer image ({}) to folder - {}'.format(name, path)
        logging.info(text)
        return text

    ### Fail
    @staticmethod
    def failShot():
        head()
        text = 'Camera; Fail; shot'
        logging.error(text)
        return text

    @staticmethod
    def failTransferImage(fileData, servData, trySend = 0):
        head()
        text = 'Camera; Fail; Try {} transfer image ({}) to server - {}'.format(trySend, fileData, servData)
        logging.error(text)
        return text

    @staticmethod
    def failDeleteImageHost(fileData):
        head()
        text = 'Camera; Fail; delete image ({}) on host'.format(fileData)
        logging.error(text)
        return text

    @staticmethod
    def failOpenStream():
        head()
        text = 'Camera; Fail; open stream'
        logging.error(text)
        return text

    @staticmethod
    def failMoveImage(name, path):
        head()
        text = 'Camera; Fail; Transfer image ({}) to folder - {}'.format(name, path)
        logging.error(text)
        return text

# Data Collect
class DataCollect(object):

    ### Sucess
    @staticmethod
    def sucess(fileData):
        head()
        logging.info(fileData)
        return fileData

    ### Fail
    @staticmethod
    def fail():
        head()
        text = 'Data Collect: Fail'
        logging.error(text)
        return text

# Serial port
class SerialPort():

    @staticmethod
    def closePort():
        head()
        text = 'Serial Port: Sucess: Close'
        logging.info(text)
        return text

    ### Suscess
    @staticmethod
    def sucessOpenPort():
        head()
        text = 'Serial Port: Sucess: Open'
        logging.info(text)
        return text


    ### Fail
    @staticmethod
    def failOpenPort():
        head()
        text = 'Data Collect: Fail; Open'
        logging.error(text)
        return text

# Folders
class Folder():
    #Success
    @staticmethod
    def sucessFolderCreateSQLError(path):
        head()
        text = 'Folder; Success; Create folder {}'.format(path)
        logging.error(text)
        return text

    # Fail
    @staticmethod
    def failFolderCreateSQLError(path):
        head()
        text = 'Folder; Fail; Create folder {}'.format(path)
        logging.error(text)
        return text
